import React from "react";
import PropTypes from "prop-types";

const A = (props) => {
  const { children, href } = props;

  return (
    <a
      target="_blank"
      className="font-semibold text-cyan-700 hover:text-cyan-600"
      href={href}
      rel="noopener"
    >
      {children}
    </a>
  );
};

A.propTypes = {};

export default A;
