import React from "react";
import NextImage from "next/image";
import s from "./Image.module.scss";

const Image = ({ className = "", ...props }) => {
  return (
    <img
      alt="vinlt blog"
      className={`${className} w-full h-auto object-cover object-center`}
      {...props}
    ></img>
  );
};

Image.propTypes = {};

export default Image;
