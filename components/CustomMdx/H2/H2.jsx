// Libraries
import React from "react";
import PropTypes from "prop-types";

const H2 = (props) => {
  const { children, id } = props;

  const link = `#${id}`;

  return (
    <h2 className="relative group py-2">
      <span id={id} className=" absolute -top-24"></span>
      {children}
      <a
        href={link}
        className="opacity-0 ml-1 group-hover:opacity-100 hover:text-cyan-700 transition-all text-gray-500"
      >
        #
      </a>
    </h2>
  );
};

H2.propTypes = {};

export default H2;
