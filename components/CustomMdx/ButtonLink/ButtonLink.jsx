import React from "react";
import PropTypes from "prop-types";

const ButtonLink = ({ href, children, icon }) => {
  return (
    <a
      className="py-1 px-2 flex items-center space-x-2 border border-cyan-700 text-cyan-700 w-max duration-300 hover:bg-cyan-700 hover:text-white rounded-md"
      target="_blank"
      rel="noopener"
      href={href}
    >
      {icon ? icon : null}
      <span>{children}</span>
    </a>
  );
};

ButtonLink.propTypes = {};

export default ButtonLink;
