import React from "react";
import PropTypes from "prop-types";

const Introduction = ({ children, ...props }) => {
  return <div>{children}</div>;
};

Introduction.propTypes = {};

export default Introduction;
