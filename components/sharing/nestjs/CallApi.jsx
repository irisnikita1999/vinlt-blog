// Libraries
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

// Constants
import { NEST_API } from "constants";
import axios from "node_modules/axios/index";

// Icons
import { LoadingIcon } from "components/icons";

const CallApi = (props) => {
  const { endpoint, method, body = {} } = props;
  const [newEndpoint, setNewEndpoint] = useState("");
  const [newBody, setNewBody] = useState({});
  const [data, setData] = useState({});
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setNewEndpoint(`${NEST_API}/${endpoint}`);
  }, [endpoint]);

  useEffect(() => {
    setNewBody(body);
  }, []);

  const onClickCallApi = async () => {
    setLoading(true);

    try {
      const { data = {} } = await axios({
        method: method,
        url: newEndpoint,
        data: { ...newBody },
      });

      setData(data);
    } catch (error) {
      if (error.response) {
        if (Object.keys(error.response.data).length) {
          setData(error.response.data);
        }
      }
    } finally {
      setLoading(false);
    }
  };

  const renderJsonView = () => {
    if (typeof window === "undefined") {
      return null;
    }
    const ReactJson = require("react-json-view").default;

    return (
      <ReactJson
        src={data}
        name={false}
        displayObjectSize={false}
        displayDataTypes={false}
      />
    );
  };

  const onChangeInput = (e) => {
    const { value } = e.target;

    setNewEndpoint(`${value}`);
  };

  const onChangeRequestBody = (key, e) => {
    const { value } = e.target;

    setNewBody({
      ...newBody,
      [key]: value,
    });
  };

  return (
    <div className="w-full my-5">
      <div className="font-semibold mb-2">Test api:</div>
      <div className="flex items-center">
        <div className="h-10 w-40 border border-gray-300 border-r-0 font-semibold flex items-center justify-center uppercase">
          {method}
        </div>
        <label className="w-full">
          <input
            type="text"
            className="h-10 px-5 outline-none focus:border-cyan-700 transition-all w-full border border-gray-300"
            value={newEndpoint}
            onChange={onChangeInput}
          />
        </label>
        <button
          disabled={isLoading}
          className="flex-shrink-0 disabled:opacity-50 disabled:pointer-events-none transition-all flex items-center text-center bg-cyan-700 text-white h-10 px-10"
          onClick={onClickCallApi}
        >
          Call Api
        </button>
      </div>
      {Object.keys(newBody).length ? (
        <div>
          <table>
            <tbody>
              {Object.entries(newBody).map(([key, value]) => (
                <tr key={key}>
                  <td className="w-40 font-semibold">{key}</td>
                  <td>
                    <label className="w-full">
                      <input
                        className="outline-none border-none w-full"
                        value={value}
                        onChange={(e) => onChangeRequestBody(key, e)}
                      />
                    </label>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ) : null}
      <div className="mt-5 bg-gray-100 rounded-md p-5 relative">
        <div
          className={`absolute transition-all duration-300 top-0 left-0 flex w-full h-full ${
            isLoading ? "opacity-100" : "opacity-0"
          } items-center justify-center`}
          style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
        >
          <LoadingIcon
            className="text-cyan-700 animate-spin"
            width={25}
            height={25}
          />
        </div>
        {renderJsonView()}
      </div>
    </div>
  );
};

CallApi.propTypes = {};
CallApi.defaultProps = {
  endpoint: "todo-controller",
  method: "GET",
};

export default CallApi;
