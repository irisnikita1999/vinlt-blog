import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import Link from "next/link";
import { useRouter } from "next/router";

import s from "./Menu.module.scss";

// Icons
import { ChevronDown } from "components/icons";
import { actions, useAppContext } from "context";

const Menu = ({ items }) => {
  const { query = {} } = useRouter();
  const { dispatch } = useAppContext();
  const { slugs = [] } = query;
  const currentSlug = slugs[slugs.length - 1];
  const [newItems, setNewItems] = useState([]);

  useEffect(() => {
    window.addEventListener("click", toggleOffSidebar);

    return () => {
      window.removeEventListener("click", toggleOffSidebar);
    };
  }, []);

  useEffect(() => {
    const draft = items.map((item) => {
      let draftItem = { ...item };

      if (item.child && item.child.length) {
        const child = item.child.map((itemChild) => ({
          ...itemChild,
          isActive: currentSlug === itemChild.name ? true : false,
        }));

        draftItem.child = child;
        draftItem.isExpand = child.some((c) => c.isActive) ? true : false;
      }

      draftItem.isActive = currentSlug === draftItem.name ? true : false;

      return draftItem;
    });

    setNewItems(draft);
  }, [items, currentSlug]);

  const toggleOffSidebar = () => {
    dispatch({ type: actions.TOGGLE_SIDEBAR, payload: false });
  };

  const onClickGroup = (index) => {
    const draftItems = newItems.map((item, idx) => ({
      ...item,
      isExpand: index === idx ? !item.isExpand : item.isExpand,
    }));

    setNewItems(draftItems);
  };

  const onClickNavigate = () => {
    dispatch({ type: actions.TOGGLE_SIDEBAR });
  };

  const renderItem = (items) => {
    return Array.isArray(items) && items.length
      ? items.map(
          ({ child, name, label, isExpand, link, isActive, props }, index) => {
            if (Array.isArray(child)) {
              return (
                <div
                  key={name}
                  className={cn(s["__group"], {
                    [s["--expand"]]: isExpand,
                  })}
                >
                  <div
                    className={cn(s["__title"], {
                      [s["--active"]]: isActive,
                    })}
                    onClick={() => onClickGroup(index)}
                  >
                    {link ? (
                      <Link href={link}>
                        <a onClick={onClickNavigate} {...props}>
                          {label}
                        </a>
                      </Link>
                    ) : (
                      <>
                        <span>{label}</span>
                        {child.length ? (
                          <ChevronDown
                            className={s["__icon-down"]}
                            width={20}
                            height={20}
                          />
                        ) : null}
                      </>
                    )}
                  </div>
                  <div className={s["__box-child"]}>{renderItem(child)}</div>
                </div>
              );
            } else {
              return (
                <Link key={name} href={link}>
                  <a
                    onClick={onClickNavigate}
                    className={cn(s["__item-child"], {
                      [s["--active"]]: isActive,
                    })}
                    {...props}
                  >
                    {label}
                  </a>
                </Link>
              );
            }
          }
        )
      : null;
  };

  return (
    <div
      className={s["menu-wrap"]}
      onClick={(e) => {
        e.stopPropagation();
      }}
    >
      {renderItem(newItems)}
    </div>
  );
};

Menu.propTypes = {};
Menu.defaultProps = {
  items: [
    {
      name: "introduction",
      label: "Giới thiệu",
      link: "",
      child: [],
    },
    {
      name: "over-view",
      label: "Tổng quan",
      child: [
        {
          name: "setting",
          label: "Cài đặt",
          link: "/nestjs/over-view/setting",
        },
      ],
    },
  ],
};

export default Menu;
