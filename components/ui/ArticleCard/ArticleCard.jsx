// Components
import React from "react";
import moment from "moment";
import Link from "next/link";
import Image from "next/image";
import { motion } from "framer-motion";

import { event } from "lib/ga";

const cardVariants = {
  offscreen: {
    rotate: -10,
    y: 100,
  },
  onscreen: {
    y: 0,
    rotate: 0,
    transition: {
      delay: 0,
      type: "spring",
      bounce: 0.4,
      duration: 2.5,
    },
  },
};

const ArticleCard = ({ cardInfo }) => {
  const { thumbnail, title, date, description, id, href = "" } = cardInfo;

  const onClickReadMore = () => {
    try {
      event({
        action: "Click read more post",
        params: {
          post_title: title,
          post_url: href,
        },
      });
    } catch (error) {
      //
    }
  };

  return (
    <motion.div
      initial="offscreen"
      whileInView="onscreen"
      viewport={{ once: true, amount: 0.5 }}
      variants={cardVariants}
      className={`post-card rounded-lg shadow-custom overflow-hidden flex flex-col h-full`}
    >
      <motion.figure
        className="relative w-full h-56"
        layoutId={`article-image-${id}`}
        transition={{ duration: 0.5 }}
      >
        <Image
          priority
          layout="fill"
          objectPosition="center"
          objectFit="cover"
          src={thumbnail}
          alt={title}
        />
      </motion.figure>
      <div className="flex flex-col text-left p-4 justify-between flex-1">
        <div>
          <motion.h2
            className="mb-2 text-xl text-gray-900 font-semibold h-14 truncate-2"
            // layoutId={`article-title-${id}`}
          >
            {title}
          </motion.h2>
          <motion.div
            className="mb-2 text-cyan-800 text-sm"
            // layoutId={`article-time-${id}`}
          >
            {moment(date, "DD-MM-YYYY").format("LL")}
          </motion.div>
          <div className="mb-2 truncate-3">{description}</div>
        </div>
        <Link href={href}>
          <a
            className="outline-none mt-2 flex items-center text-lg border-none font-semibold text-cyan-700 hover:text-cyan-600 duration-300 w-max cursor-pointer"
            onClick={onClickReadMore}
          >
            Đọc thêm
          </a>
        </Link>
      </div>
    </motion.div>
  );
};

ArticleCard.defaultProps = {
  cardInfo: {},
};

export default ArticleCard;
