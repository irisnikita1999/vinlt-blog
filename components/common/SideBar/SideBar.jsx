import React from "react";
import { Menu } from "components/ui";

const SideBar = ({ sideBar }) => {
  const items = [
    {
      name: "introduction",
      label: "Giới thiệu",
      link: "/nestjs/introduction/",
      child: [],
    },
    {
      name: "over-view",
      label: "Tổng quan",
      child: [
        {
          name: "setting",
          label: "Cài đặt",
          link: "/nestjs/over-view/setting/",
        },
        {
          name: "modules",
          label: "Modules",
          link: "/nestjs/over-view/modules/",
        },
        {
          name: "controllers",
          label: "Controllers",
          link: "/nestjs/over-view/controllers/",
        },
        {
          name: "providers",
          label: "Providers",
          link: "/nestjs/over-view/providers/",
        },
        {
          name: "middleware",
          label: "Middleware",
          link: "/nestjs/over-view/middleware/",
        },
      ],
    },
    {
      name: "summary",
      label: "Tổng kết",
      child: [],
      link: "/nestjs/summary/",
    },
    {
      name: "github",
      label: "Github",
      child: [],
      link: "https://github.com/irisnikita/nestjs-sharing",
      props: {
        target: "_blank",
        rel: "noopener",
      },
    },
  ];

  return <Menu items={sideBar} />;
};

SideBar.propTypes = {};

export default SideBar;
