// Libraries
import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import PropTypes from "prop-types";

// Style
import s from "./Header.module.scss";
import MenuIcon from "components/icons/MenuIcon";
import { actions, useAppContext } from "context";

const headerMenu = [
  { name: "posts", label: "Bài viết", href: "/posts" },
  { name: "sharing", label: "Chia sẻ", href: "/sharing" },
];

const Header = ({ sharingHeader = {} }) => {
  const { route } = useRouter();
  const { dispatch } = useAppContext();

  const onClickToggleSideBar = (e) => {
    e.stopPropagation();
    dispatch({ type: actions.TOGGLE_SIDEBAR });
  };

  const onClickToggleToc = (e) => {
    e.stopPropagation();
    dispatch({ type: actions.TOGGLE_TOC });
  };

  return (
    <div className={s["header-wrap"]}>
      <div className={s["__content"]}>
        <div className="flex lg:space-x-0 space-x-5 items-center cursor-pointer">
          <MenuIcon
            className="block lg:hidden cursor-pointer"
            onClick={onClickToggleSideBar}
          />
          <Link href="/">
            <a>
              <img
                src="/images/nltruongvi-logo.png"
                width="120"
                height="35.56"
                alt="Nestjs sharing nltv"
              />
            </a>
          </Link>
        </div>
        {Object.keys(sharingHeader).length ? (
          <div className="flex items-center space-x-2">
            <h2 className="font-semibold text-lg md:block hidden">
              {sharingHeader.title}
            </h2>
            <img
              src={sharingHeader.logo}
              width="30"
              height="28.97"
              alt="nestjs logo"
              className="cursor-pointer"
              onClick={onClickToggleToc}
            />
          </div>
        ) : (
          <div className="flex items-center space-x-5">
            {Array.isArray(headerMenu)
              ? headerMenu.map((item) => (
                  <Link key={item.name} href={item.href}>
                    <a
                      className={`hover:text-cyan-600 duration-300 ${
                        route && route.match(item.href)
                          ? "text-cyan-600 font-semibold"
                          : ""
                      }`}
                    >
                      {item.label}
                    </a>
                  </Link>
                ))
              : null}
          </div>
        )}
      </div>
    </div>
  );
};

Header.propTypes = {};

export default Header;
