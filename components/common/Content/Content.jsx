import React, { useEffect } from "react";

// Components
import { SideBar } from "components/common";
import { actions, useAppContext } from "context";

const Content = ({ children, sideBar, toc, mdx }) => {
  const { state, dispatch } = useAppContext();
  const { isOpenSideBar = false, isOpenToc = false } = state;

  useEffect(() => {
    window.addEventListener("click", toggleOffToc);

    return () => {
      window.removeEventListener("click", toggleOffToc);
    };
  }, []);

  const toggleOffToc = () => {
    dispatch({ type: actions.TOGGLE_TOC, payload: false });
  };

  return (
    <div className="max-w-7xl mx-auto px-5 py-7 flex lg:space-x-5">
      {Object.keys(sideBar).length ? (
        <div
          className={`w-60 z-20 flex-shrink-0 lg:sticky transform lg:translate-x-0 fixed lg:bg-white bg-gray-100 left-0 top-20 lg:p-0 p-5 lg:top-28 ${
            !isOpenSideBar ? "-translate-x-full" : "translate-x-0"
          } duration-300`}
          style={{ height: "calc(100% - 5rem)" }}
        >
          <SideBar sideBar={sideBar} />
        </div>
      ) : null}
      {children}
      {mdx ? (
        <div
          className="w-full overflow-hidden"
          style={{ scrollBehavior: "smooth" }}
        >
          {mdx}
        </div>
      ) : null}

      {Array.isArray(toc) && toc.length ? (
        <div
          className={`w-60 z-20 flex-shrink-0 lg:sticky transform lg:translate-x-0 fixed lg:bg-white bg-gray-100 right-0 top-20 lg:p-0 p-5 lg:top-28 ${
            !isOpenToc ? "translate-x-full" : "translate-x-0"
          } duration-300 flex flex-col space-y-1 text-right`}
          style={{ height: "calc(100% - 5rem)" }}
        >
          {toc.map(({ title, link }) => (
            <a
              key={link}
              href={link}
              className="font-semibold text-cyan-700 hover:text-cyan-600"
            >
              {title}
            </a>
          ))}
        </div>
      ) : null}
    </div>
  );
};

Content.propTypes = {};
Content.defaultProps = {
  isShowSideBar: false,
  sideBar: {},
  toc: [],
};

export default Content;
