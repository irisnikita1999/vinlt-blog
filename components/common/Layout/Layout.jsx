import React, { useEffect } from "react";
import { NextSeo, DefaultSeo } from "next-seo";
import PropTypes from "prop-types";

// Default Seo
import Seo from "next-seo.config";

// Components
import { Header, Content } from "components/common";

const Layout = ({ children, seo, sideBar, toc, mdx, sharingHeader }) => {
  return (
    <div>
      <NextSeo {...seo} />
      <DefaultSeo {...Seo} />
      <Header sharingHeader={sharingHeader} />
      <Content sideBar={sideBar} toc={toc} mdx={mdx}>
        {children}
      </Content>
    </div>
  );
};

Layout.propTypes = {};
Layout.defaultProps = {
  seo: {
    title: "Vinlt | Chia sẻ kiến thức lập trình",
    description:
      "Vinlt blog - Chia sẻ kiến thức lập trình. ReactJs, NextJs, Html, css, javascript",
    openGraph: {
      url: "",
      title: "Vinlt | Chia sẻ kiến thức lập trình",
      description:
        "Vinlt blog - Chia sẻ kiến thức lập trình. ReactJs, NextJs, Html, css, javascript",
      images: [
        {
          url: "/images/avatar.jpg",
          alt: "Vinlt | Chia sẻ kiến thức lập trình",
        },
      ],
    },
  },
  isShowSideBar: false,
};

export default Layout;
