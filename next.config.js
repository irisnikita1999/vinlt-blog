const withMDX = require("@next/mdx")();

module.exports = withMDX({
  async redirects() {
    return [
      {
        source: "/nestjs/introduction",
        destination: "/sharing/nestjs/introduction",
        permanent: true,
      },
    ];
  },
  images: {
    domains: ["strapi.vinlt.xyz", "blog-cms.vinlt.xyz"],
  },
  webpack: (config, { isServer }) => {
    if (!isServer) {
      // don't resolve 'fs' module on the client to prevent this error on build --> Error: Can't resolve 'fs'
      config.resolve.fallback = {
        fs: false,
        path: false,
      };
    }

    return config;
  },
  pageExtensions: ["js", "mdx", "jsx"],
});
