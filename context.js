import { createContext, useContext, useReducer } from "react";

export const AppContext = createContext();

export const actions = {
  TOGGLE_SIDEBAR: "TOGGLE_SIDEBAR",
  TOGGLE_TOC: "TOGGLE_TOC",
  SET_TOC: "SET_TOC",
};

let initialState = {
  isOpenSideBar: false,
  isOpenToc: false,
  toc: [],
};

function appReducer(state, action) {
  switch (action.type) {
    case actions.TOGGLE_SIDEBAR:
      return {
        ...state,
        isOpenSideBar:
          typeof action.payload === "boolean"
            ? action.payload
            : !state.isOpenSideBar,
      };
    case actions.TOGGLE_TOC:
      return {
        ...state,
        isOpenToc:
          typeof action.payload === "boolean"
            ? action.payload
            : !state.isOpenToc,
      };
    case actions.SET_TOC:
      return {
        ...state,
        toc: action.payload,
      };

    default:
      return { ...state };
  }
}

export function AppContextWrapper({ children }) {
  const [state, dispatch] = useReducer(appReducer, initialState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}
