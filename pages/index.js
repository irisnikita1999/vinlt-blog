import { Layout } from "components/common";
import { getPostList } from "services/post";
import ArticleCard from "components/ui/ArticleCard/ArticleCard";
import { Post } from "models/post";
import { useMemo } from "react";

export async function getStaticProps(context) {
  let posts = await getPostList({
    _sort: "created_at:DESC",
  });

  return {
    props: {
      posts,
    },
    revalidate: 30,
  };
}

export default function Home({ posts }) {
  const newPosts = useMemo(() => {
    return posts.map((post) => new Post(post));
  }, [posts]);

  return (
    <Layout>
      <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-5 py-5">
        {Array.isArray(newPosts)
          ? newPosts.map((cardInfo, index) => (
              <div key={cardInfo.title + index}>
                <ArticleCard cardInfo={cardInfo}></ArticleCard>
              </div>
            ))
          : null}
      </div>
    </Layout>
  );
}
