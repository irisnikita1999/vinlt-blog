import React from "react";
import matter from "gray-matter";
import dynamic from "next/dynamic";
import moment from "moment";
import { MDXRemote } from "next-mdx-remote";
import { serialize } from "next-mdx-remote/serialize";
import { MDXProvider } from "@mdx-js/react";

// Components Sharing
const Introduction = dynamic(() =>
  import("components/sharing/nestjs/Introduction")
);
const CallApi = dynamic(() => import("components/sharing/nestjs/CallApi"));
const H2 = dynamic(() => import("components/CustomMdx/H2"));
const A = dynamic(() => import("components/CustomMdx/A"));
const Image = dynamic(() => import("components/CustomMdx/Image"));
const ButtonLink = dynamic(() => import("components/CustomMdx/ButtonLink"));

// Icon
const GithubIcon = dynamic(() => import("components/icons/GithubIcon"));
const CalendarIcon = dynamic(() => import("components/icons/CalendarIcon"));

// Lib
import { getSharingData, getAllSharingPath } from "lib/sharing";
import { getHeadings } from "lib";

const components = { Introduction, CallApi, Image, GithubIcon, ButtonLink };

const mdxProviderComponents = {
  h2: H2,
  a: A,
};

// Remark
import remarkSlug from "remark-slug";
import remarkPrism from "remark-prism";
import { Layout } from "components/common/index";

export async function getStaticPaths() {
  const paths = getAllSharingPath();

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const { slugs = [] } = params;
  const { parentData, sharingContent } = await getSharingData(slugs);

  const { data, content } = matter(sharingContent);

  const mdxSource = await serialize(content, {
    // Optionally pass remark/rehype plugins
    mdxOptions: {
      remarkPlugins: [remarkPrism, remarkSlug],
      rehypePlugins: [],
    },
    scope: data,
  });

  const seo = {
    title: `${data.title} | ${parentData.title}` || "",
    description: data.description || "",
    openGraph: {
      url: "",
      title: `${data.title} | ${parentData.title}`,
      description: data.description || "",
      images: [
        {
          url: `${parentData.thumbnail}`,
          alt: `${parentData.title}`,
        },
      ],
    },
  };

  const headings = getHeadings(content);
  return {
    props: {
      source: mdxSource,
      frontMatter: data,
      seo,
      headings,
      sideBar: parentData.sideBar,
      sharingHeader: parentData.header,
    },
  };
}

const SharingArticle = ({
  source,
  frontMatter,
  seo,
  sideBar,
  sharingHeader,
  headings,
}) => {
  return (
    <Layout
      seo={seo}
      sideBar={sideBar}
      toc={headings}
      sharingHeader={sharingHeader}
      mdx={
        <>
          <div className="flex items-center justify-between mb-5 flex-wrap space-y-1">
            <h1 className="font-bold text-2xl uppercase">
              {frontMatter.title}
            </h1>
            <div className="text-gray-600 flex items-center space-x-2">
              <CalendarIcon width={18} height={18} />
              <span>
                {moment(frontMatter.date, "DD-MM-YYYY")
                  .locale("vi")
                  .format("LL")}
              </span>
            </div>
          </div>
          <section className="mdx-wrapper">
            <MDXProvider components={mdxProviderComponents}>
              <MDXRemote {...source} components={components} />
            </MDXProvider>
          </section>
        </>
      }
    ></Layout>
  );
};

SharingArticle.propTypes = {};

export default SharingArticle;
