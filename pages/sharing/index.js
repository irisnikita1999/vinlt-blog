import React from "react";
import { getAllSharing } from "lib/sharing";
import ArticleCard from "components/ui/ArticleCard/ArticleCard";
import { Layout } from "components/common";

export async function getStaticProps() {
  const sharingList = getAllSharing();

  return {
    props: {
      sharingList,
    },
  };
}

const Sharing = ({ sharingList }) => {
  return (
    <Layout>
      <div className="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-5 py-5 px-1">
        {Array.isArray(sharingList)
          ? sharingList.map((cardInfo, index) => (
              <div key={cardInfo.title + index}>
                <ArticleCard cardInfo={cardInfo}></ArticleCard>
              </div>
            ))
          : null}
      </div>
    </Layout>
  );
};

Sharing.propTypes = {};

export default Sharing;
