import React, { useMemo } from "react";
import { serialize } from "next-mdx-remote/serialize";
import { MDXRemote } from "next-mdx-remote";
import { MDXProvider } from "@mdx-js/react";
import { getPost, getSlugs } from "services/post";
import dynamic from "next/dynamic";
import NextImage from "next/image";
import moment from "moment";
import { motion } from "framer-motion";
import { useRouter } from "next/router";

import { Post } from "models/post";

// Remark
import remarkSlug from "remark-slug";
import remarkPrism from "remark-prism";
import { getHeadings, getStrapMedia } from "lib/index";
import { Layout } from "components/common/index";

// Components Sharing
const Introduction = dynamic(() =>
  import("components/sharing/nestjs/Introduction")
);
const CallApi = dynamic(() => import("components/sharing/nestjs/CallApi"));
const H2 = dynamic(() => import("components/CustomMdx/H2"));
const A = dynamic(() => import("components/CustomMdx/A"));
const Image = dynamic(() => import("components/CustomMdx/Image"));
const ButtonLink = dynamic(() => import("components/CustomMdx/ButtonLink"));

// Icon
const GithubIcon = dynamic(() => import("components/icons/GithubIcon"));
const CalendarIcon = dynamic(() => import("components/icons/CalendarIcon"));
const LoadingIcon = dynamic(() => import("components/icons/LoadingIcon"));

const components = { Introduction, CallApi, Image, GithubIcon, ButtonLink };

const mdxProviderComponents = {
  h2: H2,
  a: A,
};

export async function getStaticProps({ params }) {
  const { slug } = params;

  const post = await getPost(slug);

  const mdxSource = await serialize(post.content, {
    mdxOptions: {
      remarkPlugins: [remarkPrism, remarkSlug],
      rehypePlugins: [],
    },
  });

  const seo = {
    title: `${post.title}` || "",
    description: post.description || "",
    openGraph: {
      url: "",
      title: `${post.title}`,
      description: post.description || "",
      images: [
        {
          url: `${getStrapMedia(post.image)}`,
          alt: `${post.title}`,
        },
      ],
    },
  };

  const headings = getHeadings(post.content);

  return {
    props: {
      seo,
      headings,
      post: {
        ...post,
        content: mdxSource,
      },
    },
    revalidate: 5, // will be passed to the page component as props
  };
}

export async function getStaticPaths() {
  const data = await getSlugs();

  const paths = data.map((item) => ({ params: { slug: item.slug } }));

  return {
    paths: paths,
    fallback: "blocking", // See the "fallback" section below
  };
}

const PostDetail = ({ post, seo, headings }) => {
  const router = useRouter();
  const newPost = useMemo(() => {
    return new Post(post);
  }, [post]);

  if (router.isFallback) {
    return (
      <div className="w-full h-full fixed flex items-center justify-center top-0 left-0">
        <LoadingIcon className="animate-spin" width={40} height={40} />
      </div>
    );
  }

  return (
    <Layout
      seo={seo}
      toc={headings}
      mdx={
        <>
          <motion.figure
            className="relative rounded-t-md mt-2 overflow-hidden border border-gray-200 w-full md:h-96 h-60"
            layoutId={`article-image-${newPost.id}`}
            transition={{ duration: 0.5 }}
          >
            <NextImage
              src={newPost.image}
              alt={newPost.title}
              priority
              layout="fill"
              objectFit="cover"
              objectPosition="center"
            />
          </motion.figure>
          <motion.div
            className="text-gray-600 flex items-center space-x-2 mt-2 mb-5 relative"
            animate={{
              top: [-50, 0],
              opacity: [0, 1],
            }}
            transition={{
              type: "spring",
              duration: 0.7,
              bounce: 0.4,
            }}
          >
            <CalendarIcon width={18} height={18} />
            <span>
              {moment(newPost.date, "DD-MM-YYYY").locale("vi").format("LL")}
            </span>
          </motion.div>
          <motion.h1
            animate={{
              opacity: [0, 1],
            }}
            transition={{ duration: 1 }}
            className="font-bold text-2xl uppercase mb-5"
            // layoutId={`article-title-${newPost.id}`}
          >
            {newPost.title}
          </motion.h1>
          <motion.section
            className="mdx-wrapper relative"
            animate={{ opacity: [0, 1], top: [100, 0] }}
            transition={{ duration: 1 }}
          >
            <MDXProvider components={mdxProviderComponents}>
              <MDXRemote {...newPost.content} components={components} />
            </MDXProvider>
          </motion.section>
        </>
      }
    ></Layout>
  );
};

PostDetail.propTypes = {};

export default PostDetail;
