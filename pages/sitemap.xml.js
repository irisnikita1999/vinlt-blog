import { getAllSharingSlug } from "lib/sharing";
import { getSlugs } from "services/post";

const Sitemap = () => {};

const baseUrl = "https://blog.vinlt.xyz";

const defaultSites = ["/", "/sharing", "/posts"];

export const getServerSideProps = async ({ res }) => {
  const sharingSlugs = getAllSharingSlug();
  const postSlugs = await getSlugs();

  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      ${
        Array.isArray(defaultSites)
          ? defaultSites
              .map((site) => {
                return `
            <url>
              <loc>${baseUrl + site}</loc>
              <lastmod>${new Date().toISOString()}</lastmod>
              <changefreq>monthly</changefreq>
              <priority>1.0</priority>
            </url>
          `;
              })
              .join("")
          : ""
      }
      ${
        Array.isArray(sharingSlugs)
          ? sharingSlugs
              .map((sharingSlug) => {
                return `
            <url>
              <loc>${baseUrl + "/sharing/" + sharingSlug}</loc>
              <lastmod>${new Date().toISOString()}</lastmod>
              <changefreq>monthly</changefreq>
              <priority>1.0</priority>
            </url>
          `;
              })
              .join("")
          : ""
      }
      ${
        Array.isArray(postSlugs)
          ? postSlugs
              .map((slug) => {
                return `
            <url>
              <loc>${baseUrl}/posts/${slug.slug}</loc>
              <lastmod>${new Date().toISOString()}</lastmod>
              <changefreq>monthly</changefreq>
              <priority>1.0</priority>
            </url>
          `;
              })
              .join("")
          : ""
      }
    </urlset>
  `;

  res.setHeader("Content-Type", "text/xml");
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};

export default Sitemap;
