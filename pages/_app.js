// Libraries
import { AppContextWrapper } from "context";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { AnimateSharedLayout } from "framer-motion";

import * as ga from "lib/ga";

import "tailwindcss/tailwind.css";
import "style/global.scss";
import "moment/locale/vi";

function MyApp({ Component, pageProps }) {
  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url) => {
      ga.pageview(url);
    };
    //When the component is mounted, subscribe to router changes
    //and log those page views
    router.events.on("routeChangeComplete", handleRouteChange);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  return (
    <AppContextWrapper>
      <Component {...pageProps} />
    </AppContextWrapper>
  );
}

export default MyApp;
