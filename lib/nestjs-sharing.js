import fs from "fs";
import path from "path";
import moment from "moment";
import matter from "gray-matter";

//Finding directory named "posts" from the current working directory of Node.

export const getSharing = () => {
  const nestjsSharingDirectory = path.join(process.cwd(), "sharing", "nestjs");

  const fileNames = fs.readdirSync(nestjsSharingDirectory);

  const allNestjsSharingData = fileNames.map((filename) => {
    const slug = filename.replace(".mdx", "");

    const fullPath = path.join(nestjsSharingDirectory, filename);

    //Extracts contents of the MDX file
    const fileContents = fs.readFileSync(fullPath, "utf8");
    const { data } = matter(fileContents);

    return {
      slug,
      data: {
        ...data,
        date: moment(data.moment, "DD-MM-YYYY").locale("vi").format("LLL"),
      },
    };
  });
};

//Get Post based on Slug
export const getSharingData = async (slug) => {
  const nestjsSharingDirectory = path.join(process.cwd(), "sharing", "nestjs");

  const fullPath = path.join(nestjsSharingDirectory, `${slug}.mdx`);
  const sharingContent = fs.readFileSync(fullPath, "utf8");

  return sharingContent;
};
