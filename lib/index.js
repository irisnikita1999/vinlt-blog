import get from 'lodash/get'

export const getHeadings = (source) => {
  const regex = /^(#{1,6}.*)/gm;

  if (source.match(regex)) {
    return source.match(regex).map((heading) => {
      const headingText = heading.replace(/^([^\s]*)\s/g, "");

      const link =
        "#" +
        headingText
          .replace(/(?=[\x00-\x7F])[^A-Za-z0-9 ]/g, "")
          .replace(/ /g, "-")
          .toLowerCase();

      return {
        title: headingText,
        link,
      };
    });
  }

  return [];
};

export function getStrapiURL(path = "") {
  return `${
    process.env.NEXT_PUBLIC_STRAP_API || "http://localhost:1337"
  }${path}`;
}


export function getStrapMedia(media = {}) {
  const newMediaUrl = get(media, 'url', '')

  const imageUrl = newMediaUrl.startsWith('/')
    ? getStrapiURL(newMediaUrl)
    : newMediaUrl === '' ? '/img/bg-img/13.jpg' : media.url
  return imageUrl
}
