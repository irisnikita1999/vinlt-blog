import fs from "fs";
import path from "path";
import moment from "moment";
import matter from "gray-matter";

//Finding directory named "posts" from the current working directory of Node.

export const getAllSharing = () => {
  const nestjsSharingDirectory = path.join(process.cwd(), "sharing");

  const folderNames = fs.readdirSync(nestjsSharingDirectory);

  const sharingArticles = folderNames.map((folderName) => {
    const fileContents = fs.readFileSync(
      path.join(nestjsSharingDirectory, folderName, "index.mdx"),
      "utf8"
    );

    const { data } = matter(fileContents);
    return {
      ...data,
    };
  });

  return sharingArticles;
};

export const getAllSharingSlug = () => {
  const nestjsSharingDirectory = path.join(process.cwd(), "sharing");

  const folderNames = fs.readdirSync(nestjsSharingDirectory);
  const paths = [];

  folderNames.map((folderName) => {
    const fileNames = fs.readdirSync(
      path.join(nestjsSharingDirectory, folderName)
    );

    fileNames.map((fileName) => {
      if (fileName !== "index.mdx") {
        paths.push(folderName + "/" + fileName.replace(".mdx", ""));
      }
    });
  });

  return paths;
};

export const getAllSharingPath = () => {
  const nestjsSharingDirectory = path.join(process.cwd(), "sharing");

  const folderNames = fs.readdirSync(nestjsSharingDirectory);
  const paths = [];

  folderNames.map((folderName) => {
    const fileNames = fs.readdirSync(
      path.join(nestjsSharingDirectory, folderName)
    );

    fileNames.map((fileName) => {
      if (fileName !== "index.mdx") {
        paths.push({
          params: { slugs: [folderName, fileName.replace(".mdx", "")] },
        });
      }
    });
  });

  return paths;
};

export const getSharing = () => {
  const nestjsSharingDirectory = path.join(process.cwd(), "sharing", "nestjs");

  const fileNames = fs.readdirSync(nestjsSharingDirectory);

  const allNestjsSharingData = fileNames.map((filename) => {
    const slug = filename.replace(".mdx", "");

    const fullPath = path.join(nestjsSharingDirectory, filename);

    //Extracts contents of the MDX file
    const fileContents = fs.readFileSync(fullPath, "utf8");
    const { data } = matter(fileContents);

    return {
      slug,
      data: {
        ...data,
        date: moment(data.moment, "DD-MM-YYYY").locale("vi").format("LLL"),
      },
    };
  });
};

//Get Post based on Slug
export const getSharingData = async (slugs) => {
  const nestjsSharingDirectory = path.join(process.cwd(), "sharing", slugs[0]);
  const fileContents = fs.readFileSync(
    path.join(nestjsSharingDirectory, "index.mdx"),
    "utf8"
  );

  const { data } = matter(fileContents);
  const fullPath = path.join(nestjsSharingDirectory, `${slugs[1]}.mdx`);
  const sharingContent = fs.readFileSync(fullPath, "utf8");

  return {
    parentData: {
      thumbnail: data.thumbnail,
      title: data.title,
      sideBar: data.sideBar,
      header: data.header,
    },
    sharingContent,
  };
};
