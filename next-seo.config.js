export default {
  openGraph: {
    type: 'website',
    locale: 'vi_VN',
    url: 'https://www.url.ie/',
    site_name: 'Vinlt sharing',
    profile: {
      firstName: "Vi",
      lastName: "Nguyen",
      userName: "nltruongvi1999",
      gender: "male"
    }
  },
  twitter: {
    handle: '@handle',
    site: '@site',
    cardType: 'summary_large_image',
  },
};