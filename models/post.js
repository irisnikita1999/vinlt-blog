import { getStrapiURL } from "lib/index";
import moment from "moment";
import { Model } from "./index";
import get from "lodash/get";

export class Post extends Model {
  get id() {
    return this.model.id;
  }

  get title() {
    return this.model.title;
  }

  get description() {
    return this.model.description;
  }

  get content() {
    return this.model.content;
  }

  get date() {
    return moment(this.model.createdAt).format("DD-MM-YYYY");
  }

  get image() {
    const url = get(this.model, "image.formats.medium.url", "");

    if (url !== "") {
      return getStrapiURL(url);
    }

    return "/images/placeholder.jpg";
  }

  get thumbnail() {
    const url = get(this.model, "image.formats.small.url", "");

    if (url !== "") {
      return getStrapiURL(url);
    }

    return "/images/placeholder.jpg";
  }

  get href() {
    return `/posts/${this.model.slug}`;
  }
}
