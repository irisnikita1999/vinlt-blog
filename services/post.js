// Libraries
import axios from "axios";

const endpoint = process.env.NEXT_PUBLIC_STRAP_API + "/articles";

export const getPostList = async (params) => {
  try {
    const { data } = await axios.get(endpoint, { params });

    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getPost = async (slug) => {
  try {
    const { data } = await axios.get(endpoint + `/${slug}`);

    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const getSlugs = async () => {
  try {
    const { data } = await axios.get(endpoint + "/slugs");

    return data;
  } catch (error) {
    return Promise.reject(error);
  }
};
